;OR
(define OR
	(lambda (M N)
              (lambda(a b)
		(N a (M a b)))))
;add 
(define add 
	(lambda (m n s z)
              (m s(n s z))))
;LT
(define LT
	(lambda (a b)
		((AND( (NOT(EQ a b)) (LEQ a b) )))))
;isZero
(define isZero
	(lambda (n)
		(n (lambda(x) (false)) true)))

;define func
(define func
	(lambda (n f)
		((isZero n) n (add n f(n-1)))))

;define Y-combinator
(define Y
	(lambda (f)
		( (lambda(x)f(x x)) (lambda(x)f(x x)) )))

;define if-else
(define if-else_func
	(lambda (m n)
		(OR((isZero m) (LT m n))
			(Y func(n func))
			(add n m))))

